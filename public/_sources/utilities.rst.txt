Utilities
===========

Authentication
_______________

.. automodule:: eawagrs.utilities.Eauthenticate
   :members:
   :undoc-members:
   :show-inheritance:
   :exclude-members:

Colors
_____________

.. automodule:: eawagrs.utilities.Ecolours
   :members:
   :undoc-members:
   :show-inheritance:

Flags
_____________

.. automodule:: eawagrs.utilities.Eflags
   :members:
   :undoc-members:
   :show-inheritance:

Luts
_____________

.. automodule:: eawagrs.utilities.Eluts
   :members:
   :undoc-members:
   :show-inheritance:

Stats
_____________

.. automodule:: eawagrs.utilities.Estats
   :members:
   :undoc-members:
   :show-inheritance:

Assorted Utils
_________________

.. automodule:: eawagrs.utilities.Eutils
   :members:
   :undoc-members:
   :show-inheritance:
