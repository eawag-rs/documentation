# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='Eawag-RS',
    version='0.1.0',
    description='Python repository of the Eawag Remote Sensing group.',
    long_description=readme,
    author='Daniel Odermatt',
    author_email='Daniel.Odermatt@eawag.ch',
    url='https://renkulab.io/gitlab/odermatt/eawag-rs',
    license=license,
    packages=find_packages(exclude=('sandbox', 'docs'))
)