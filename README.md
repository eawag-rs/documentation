# Eawag Remote Sensing Group Python Library
## Introduction

This repository contains a collection of python modules for processing and visualising satellite and insitu data, 
written and curated by the [Remote Sensing Group](https://www.eawag.ch/en/department/surf/main-focus/remote-sensing/)
at [Eawag](https://www.eawag.ch/) led by [Daniel Odermatt](https://www.eawag.ch/en/aboutus/portrait/organisation/staff/profile/daniel-odermatt/show/). 

## Installation

- Clone the repository to your local machine using the command: 

 `git clone git@renkulab.io:odermatt/eawag-rs.git`
 
 Note that the repository will be copied to your current working directory.

- Use Python 3 and install the requirements with:

 `pip install -r requirements.txt`

 The python version can be checked by running the command `python --version`. In case python is not installed or only an older version of it, it is recommend to install python through the anaconda distribution which can be downloaded [here](https://www.anaconda.com/products/individual). 

## Usage

This repository contains a wide array of modules for various remote sensing related data processing tasks.
Please refer to the documentation for a list of usable functions or feel free to browse the code. 

Functions can be called from the package as follows:

```python
from eawagrs.insitu.cdom import cdom

cuvette_size = 0.05  # 5cm cuvette. It must be changed in case of using other sizes.

CDOM = cdom()
CDOM.read_data_cary100("input/example_data.csv", cuvette_size)
CDOM.plot_data("output/output_example.png")
CDOM.to_csv("output/output_example.csv")
df = CDOM.get_df()
```

## Collaboration

For any issues or suggestions regarding scripts found in this repository please open a new issue. 
